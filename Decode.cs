using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System;
using Wasm;

class UnknownValueTypeException : Exception { }
class UnknownImportException : Exception { }
class InvalidOpcodeException : Exception { }
class NotWasmFileException : Exception { }
class EarlyEndException : Exception { }

class WasmDecoder
{
	DecodeCode DC;
	Decoder D;
	
	//cmfnjZgz
	public WasmDecoder(byte[] data)
	{
		this.D = new Decoder(data);
		this.DC = new DecodeCode(D);
	}
	
	public File parse()
	{
		// "\0asm"
		if(D.parseUint32() != 0x6d736100) {
			throw new NotWasmFileException();
		}
		
		File file = new File();
		file.version = D.parseUint32();
		file.customSections = new List<CustomSection>();
		
		for(byte? sectionId = D.tryNext();
			sectionId != null;
			sectionId = D.tryNext())
		{
			uint cursorEnd = D.parseVaruint32() + D.cursor;
			
			switch((SectionKind)sectionId)
			{
				case SectionKind.CUSTOM:
				{
					file.customSections.Add(new CustomSection(D.parseString(D.parseVaruint32())));
					D.cursor = cursorEnd;
				} break;
				case SectionKind.TYPE:
				{
					file.functionSignatures = new FunctionSignature[D.parseVaruint32()];
					for(int i = 0; i < file.functionSignatures.Length; i += 1) {
						file.functionSignatures[i] = parseFuncType();
					}
				} break;
				case SectionKind.IMPORT:
				{
					file.imports = new Import[D.parseVaruint32()];
					for(int i = 0; i < file.imports.Length; i += 1) {
						file.imports[i] = parseImport();
					}
				} break;
				case SectionKind.FUNCTION:
				{
					file.functionTypes = new uint[D.parseVaruint32()];
					for(int i = 0; i < file.functionTypes.Length; i += 1) {
						file.functionTypes[i] = D.parseVaruint32();
					}
				} break;
				case SectionKind.TABLE:
				{
					file.tableTypes = new TableType[D.parseVaruint32()];
					for(int i = 0; i < file.tableTypes.Length; i += 1) {
						file.tableTypes[i] = parseTableType();
					}
				} break;
				case SectionKind.MEMORY:
				{
					file.memoryEntries = new ResizableLimit[D.parseVaruint32()];
					for(int i = 0; i < file.memoryEntries.Length; i += 1) {
						file.memoryEntries[i] = parseMemoryLimit();
					}
				} break;
				case SectionKind.GLOBAL:
				{
					file.globals = new Global[D.parseVaruint32()];
					for(int i = 0; i < file.globals.Length; i += 1) {
						file.globals[i] = new Global(
							parseGlobalType(), DC.decodeNext()
						);
					}
				} break;
				case SectionKind.EXPORT:
				{
					file.exports = new Export[D.parseVaruint32()];
					for(int i = 0; i < file.exports.Length; i += 1) {
						file.exports[i] = new Export(
							D.parseString(D.parseVaruint32()),
							(ExternalKind)D.next(),
							D.parseVaruint32()
						);
					}
				} break;
				case SectionKind.START:
				{
					file.startFunction = D.parseVaruint32();
				} break;
				// case SectionKind.ELEMENT:
				// {
				// 	uint count = D.parseVaruint32();
				// } break;
				case SectionKind.CODE:
				{
					file.functionBodies = new FunctionBody[D.parseVaruint32()];
					for(int i = 0; i < file.functionBodies.Length; i += 1) {
						file.functionBodies[i] = parseFunctionBody();
					}
				} break;
				// case SectionKind.DATA:
				// {
				// 	uint count = D.parseVaruint32();
				// } break;
				default:
					Console.WriteLine($"Unknown section id: {sectionId}");
					D.cursor = cursorEnd;
					break;
			}
			if(cursorEnd != D.cursor) {
				D.cursor = cursorEnd;
				Console.WriteLine($"{(SectionKind)sectionId} section not properly parsed");
			}
		}
		
		return file;
	}
	
	FunctionSignature parseFuncType()
	{
		byte form = D.parseVarint7();
		Wasm.ValueType[] paramaters = new Wasm.ValueType[D.parseVaruint32()];
		for(int i = 0; i < paramaters.Length; i += 1) {
			paramaters[i] = D.parseValueType();
		}
		Wasm.ValueType[] returns = new Wasm.ValueType[D.parseI1() ? 1 : 0];
		if(returns.Length > 0) {
			returns[0] = D.parseValueType();
		}
		return new FunctionSignature(form, paramaters, returns);
	}
	
	Import parseImport()
	{
		string module = D.parseString(D.parseVaruint32()),
		       field  = D.parseString(D.parseVaruint32());
		
		switch(D.next())
		{
		case 0: return new ImportFunctionType(module, field, D.parseVaruint32());
		case 1: return new ImportTableType(module, field, parseTableType());
		case 2: return new ImportMemoryType(module, field, parseMemoryLimit());
		case 3: return new ImportGlobalType(module, field, parseGlobalType());
		default: throw new UnknownImportException();
		}
	}
	
	FunctionBody parseFunctionBody()
	{
		uint codeEnd = D.parseVaruint32() + D.cursor;
		
		Local[] locals = new Local[D.parseVaruint32()];
		for(int i = 0; i < locals.Length; i += 1) {
			locals[i] = new Local(D.parseVaruint32(), D.parseValueType());
		}
		var code = DC.decode(codeEnd);
		D.cursor = codeEnd;
		return new FunctionBody(locals, code);
	}
		
	GlobalType parseGlobalType()
		=> new GlobalType(D.parseValueType(), D.parseI1());
	
	TableType parseTableType()
		=> new TableType(D.parseValueType(), parseMemoryLimit());
	
	ResizableLimit parseMemoryLimit()
	{
		bool hasMaximum = D.parseI1();
		return new ResizableLimit(D.parseVaruint32(), hasMaximum
			? (uint?)D.parseVaruint32()
			: null);
	}	
}

class Decoder
{
	public byte[] data;
	public uint cursor;
	
	readonly Dictionary<byte, Wasm.ValueType> valueCodes = new Dictionary<byte, Wasm.ValueType>() {
		{ 0x7f, Wasm.ValueType.i32        },
		{ 0x7e, Wasm.ValueType.i64        },
		{ 0x7d, Wasm.ValueType.f32        },
		{ 0x7c, Wasm.ValueType.f64        },
		{ 0x70, Wasm.ValueType.any_func   },
		{ 0x60, Wasm.ValueType.func       },
		{ 0x40, Wasm.ValueType.block_type },
	};
	
	public Decoder(byte[] data)
	{
		this.data = data;
	}
	
	public string parseString(uint length)
	{
		string result = System.Text.Encoding.UTF8.GetString(data, (int)cursor, (int)length);
		cursor += length;
		return result;
	}
	
	public Wasm.ValueType parseValueType()
	{
		Wasm.ValueType code;
		if(valueCodes.TryGetValue(parseVarint7(), out code)) {
			return code;
		}
		throw new UnknownValueTypeException();
	}
	
	public byte next() {
		if(cursor < data.Length) {
			return data[cursor++];
		}
		throw new EarlyEndException();
	}
		
	public byte? tryNext() => (cursor < data.Length)
		? (byte?)data[cursor++]
		: null;
	
	public float parseF32()
	{
		if(cursor + 4 < data.Length) {
			float val = BitConverter.ToSingle(data, (int)cursor);
			cursor += 4;
			return val;
		}
		throw new EarlyEndException();
	}
	
	public double parseF64()
	{
		if(cursor + 8 < data.Length) {
			double val = BitConverter.ToDouble(data, (int)cursor);
			cursor += 8;
			return val;
		}
		throw new EarlyEndException();
	}
	
	public uint parseUint32()
	{
		if(cursor + 4 < data.Length) {
			uint val = BitConverter.ToUInt32(data, (int)cursor);
			cursor += 4;
			return val;
		}
		throw new EarlyEndException();
	}
	
	public ulong parseUint64()
	{
		if(cursor + 8 < data.Length) {
			ulong val = BitConverter.ToUInt64(data, (int)cursor);
			cursor += 8;
			return val;
		}
		throw new EarlyEndException();
	}
	
	public bool parseI1()        => next() != 0;
	public byte parseVarint7()   => (byte)parseVarint(7);
	public uint parseVaruint32() => (uint)parseVaruint();
	public ulong parseVaruint64() => parseVaruint();
	
	ulong parseVaruint()
	{
		ulong result = 0;
		int shift = 0;
		while(true)
		{
			byte b = next();
			result |= (ulong)(b & 0x7f) << shift;
			if ((b & 0x80) == 0)
				break;
			shift += 7;
		}
		return result;
	}
	
	long parseVarint(int bits)
	{
		long result = 0;
		int shift = 0;
		byte b;
		do {
			b = next();
			result |= ((long)(b & 0x7F) << shift);
			shift += 7;
		} while((b & 0x80) != 0);
		
		return ((shift < bits) && (b & 0x40) != 0)
			? result | -(1L << shift)
			: result;
	}
}