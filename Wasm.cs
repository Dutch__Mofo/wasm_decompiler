using System.Collections.Generic;

namespace Wasm
{

public struct File
{
	public uint version;
	
	public List<CustomSection> customSections;
	public FunctionSignature[] functionSignatures;
	public Import[]            imports;
	public uint[]              functionTypes;
	public TableType[]         tableTypes;
	public ResizableLimit[]    memoryEntries;
	public Global[]            globals;
	public Export[]            exports;
	public uint?               startFunction;
	public FunctionBody[]      functionBodies;
}

public class CustomSection
{
	public CustomSection(string n) { name = n; }
	public string name;
}

public struct FunctionSignature
{
	public FunctionSignature(byte form, ValueType[] parameters, ValueType[] returns)
	{
		this.parameters = parameters;
		this.returns = returns;
		this.form = form;
	}
	public ValueType[] parameters, returns;
	public byte form;
}

public struct ResizableLimit
{
	public ResizableLimit(uint initial, uint? maximum)
	{
		this.initial = initial;
		this.maximum = maximum;
	}
	public uint initial;
	public uint? maximum;
}

public struct Global
{
	public Global(GlobalType type, Op initializer)
	{
		this.initializer = initializer;
		this.globalType = type;
	}
	public GlobalType globalType;
	public Op initializer;
}

public struct Export
{
	public Export(string field, ExternalKind kind, uint index)
	{
		this.index = index;
		this.field = field;
		this.kind = kind;
	}
	public string field;
	public ExternalKind kind;
	public uint index;
}

public struct FunctionBody
{
	public FunctionBody(Local[] locals, Op[] code)
	{
		this.locals = locals;
		this.code = code;
	}
	public Local[] locals;
	public Op[] code;
}

public struct Local
{
	public Local(uint count, ValueType type)
	{
		this.count = count;
		this.type = type;
	}
	public uint count;
	public ValueType type;
}

public class Op
{
	public Op(Opcode opcode)
	{
		this.opcode = opcode;
	}	
	public Opcode opcode;
	public override string ToString() => opcode.ToString().ToLower();
}

public class OpI1 : Op
{
	public OpI1(Opcode opcode, bool i) : base(opcode)
	{
		this.i = i;
	}	
	public bool i;
	public override string ToString() => $"{opcode} {i}".ToLower();
}

public class OpI8 : Op
{
	public OpI8(Opcode opcode, byte i) : base(opcode)
	{
		this.i = i;
	}	
	public byte i;
	public override string ToString() => opcode.ToString().ToLower() + ' ' + i;
}

public class OpI32 : Op
{
	public OpI32(Opcode opcode, uint i) : base(opcode)
	{
		this.i = i;
	}	
	public uint i;
	public override string ToString() => opcode.ToString().ToLower() + ' ' + i;
}

public class OpI64 : Op
{
	public OpI64(Opcode opcode, ulong i) : base(opcode)
	{
		this.i = i;
	}	
	public ulong i;
	public override string ToString() => opcode.ToString().ToLower() + ' ' + i;
}

public class OpF32 : Op
{
	public OpF32(Opcode opcode, float f) : base(opcode)
	{
		this.f = f;
	}	
	public float f;
	public override string ToString() => opcode.ToString().ToLower() + ' ' + f;
}

public class OpF64 : Op
{
	public OpF64(Opcode opcode, double d) : base(opcode)
	{
		this.d = d;
	}	
	public double d;
	public override string ToString() => opcode.ToString().ToLower() + ' ' + d;
}

public class OpMemImmidiate : Op
{
	public OpMemImmidiate(Opcode opcode, MemImmidiate immidiate) : base(opcode)
	{
		this.immidiate = immidiate;
	}	
	public MemImmidiate immidiate;
	public override string ToString() => opcode.ToString().ToLower() + ' ' + immidiate;
}

public struct MemImmidiate
{
	public MemImmidiate(uint flags, uint offset)
	{
		this.flags = flags;
		this.offset = offset;
	}
	public uint flags, offset;
	public override string ToString() => $"offset={offset} flags=0x{flags:x}";
}

public class OpBrTable : Op
{
	public OpBrTable(Opcode opcode, uint defaultTarget, uint[] targets) : base(opcode)
	{
		this.defaultTarget = defaultTarget;
		this.targets = targets;
	}
	public uint defaultTarget;
	public uint[] targets;
	public override string ToString() => $"{opcode.ToString().ToLower()} {defaultTarget}, [{targets.implode(", ")}]";
}

public class OpI32_I1 : Op
{
	public OpI32_I1(Opcode opcode, uint i32, bool i1) : base(opcode)
	{
		this.i32 = i32;
		this.i1 = i1;
	}	
	public uint i32;
	bool i1;
	public override string ToString() => $"{opcode} {i32} {i1}".ToLower();
}

public enum ExternalKind
{
    FUNCTION = 0,
    TABLE    = 1,
    MEMORY   = 2,
    GLOBAL   = 3,
}

public enum SectionKind
{
	CUSTOM   = 0,
	TYPE     = 1,
	IMPORT   = 2,
	FUNCTION = 3,
	TABLE    = 4,
	MEMORY   = 5,
	GLOBAL   = 6,
	EXPORT   = 7,
	START    = 8,
	ELEMENT  = 9,
	CODE     = 10,
	DATA     = 11,
}

public enum Opcode
{
	UNREACHABLE         = 0x00,
	NOP                 = 0x01,
	BLOCK               = 0x02,
	LOOP                = 0x03,
	IF                  = 0x04,
	ELSE                = 0x05,
	
	RESERVED_00         = 0x06,
	RESERVED_01         = 0x07,
	RESERVED_02         = 0x08,
	RESERVED_03         = 0x09,
	RESERVED_04         = 0x0a,
	
	END                 = 0x0b,
	BR                  = 0x0c,
	BR_IF               = 0x0d,
	BR_TABLE            = 0x0e,
	RETURN              = 0x0f,
	CALL                = 0x10,
	CALL_INDIRECT       = 0x11,
	
	RESERVED_05         = 0x12,
	RESERVED_06         = 0x13,
	RESERVED_07         = 0x14,
	RESERVED_08         = 0x15,
	RESERVED_09         = 0x16,
	RESERVED_0a         = 0x17,
	RESERVED_0b         = 0x18,
	RESERVED_0c         = 0x19,
	
	DROP                = 0x1a,
	SELECT              = 0x1b,
	
	RESERVED_0d         = 0x1c,
	RESERVED_0e         = 0x1d,
	RESERVED_0f         = 0x1e,
	RESERVED_10         = 0x1f,
	
	GET_LOCAL           = 0x20,
	SET_LOCAL           = 0x21,
	TEE_LOCAL           = 0x22,
	GET_GLOBAL          = 0x23,
	SET_GLOBAL          = 0x24,
	
	RESERVED_11         = 0x25,
	RESERVED_12         = 0x26,
	RESERVED_13         = 0x27,
	
	I32_LOAD            = 0x28,
	I64_LOAD            = 0x29,
	F32_LOAD            = 0x2a,
	F64_LOAD            = 0x2b,
	I32_LOAD8_S         = 0x2c,
	I32_LOAD8_U         = 0x2d,
	I32_LOAD16_S        = 0x2e,
	I32_LOAD16_U        = 0x2f,
	I64_LOAD8_S         = 0x30,
	I64_LOAD8_U         = 0x31,
	I64_LOAD16_S        = 0x32,
	I64_LOAD16_U        = 0x33,
	I64_LOAD32_S        = 0x34,
	I64_LOAD32_U        = 0x35,
	I32_STORE           = 0x36,
	I64_STORE           = 0x37,
	F32_STORE           = 0x38,
	F64_STORE           = 0x39,
	I32_STORE8          = 0x3a,
	I32_STORE16         = 0x3b,
	I64_STORE8          = 0x3c,
	I64_STORE16         = 0x3d,
	I64_STORE32         = 0x3e,
	CURRENT_MEMORY      = 0x3f,
	GROW_MEMORY         = 0x40,
	I32_CONST           = 0x41,
	I64_CONST           = 0x42,
	F32_CONST           = 0x43,
	F64_CONST           = 0x44,
	I32_EQZ             = 0x45,
	I32_EQ              = 0x46,
	I32_NE              = 0x47,
	I32_LT_S            = 0x48,
	I32_LT_U            = 0x49,
	I32_GT_S            = 0x4a,
	I32_GT_U            = 0x4b,
	I32_LE_S            = 0x4c,
	I32_LE_U            = 0x4d,
	I32_GE_S            = 0x4e,
	I32_GE_U            = 0x4f,
	I64_EQZ             = 0x50,
	I64_EQ              = 0x51,
	I64_NE              = 0x52,
	I64_LT_S            = 0x53,
	I64_LT_U            = 0x54,
	I64_GT_S            = 0x55,
	I64_GT_U            = 0x56,
	I64_LE_S            = 0x57,
	I64_LE_U            = 0x58,
	I64_GE_S            = 0x59,
	I64_GE_U            = 0x5a,
	F32_EQ              = 0x5b,
	F32_NE              = 0x5c,
	F32_LT              = 0x5d,
	F32_GT              = 0x5e,
	F32_LE              = 0x5f,
	F32_GE              = 0x60,
	F64_EQ              = 0x61,
	F64_NE              = 0x62,
	F64_LT              = 0x63,
	F64_GT              = 0x64,
	F64_LE              = 0x65,
	F64_GE              = 0x66,
	I32_CLZ             = 0x67,
	I32_CTZ             = 0x68,
	I32_POPCNT          = 0x69,
	I32_ADD             = 0x6a,
	I32_SUB             = 0x6b,
	I32_MUL             = 0x6c,
	I32_DIV_S           = 0x6d,
	I32_DIV_U           = 0x6e,
	I32_REM_S           = 0x6f,
	I32_REM_U           = 0x70,
	I32_AND             = 0x71,
	I32_OR              = 0x72,
	I32_XOR             = 0x73,
	I32_SHL             = 0x74,
	I32_SHR_S           = 0x75,
	I32_SHR_U           = 0x76,
	I32_ROTL            = 0x77,
	I32_ROTR            = 0x78,
	I64_CLZ             = 0x79,
	I64_CTZ             = 0x7a,
	I64_POPCNT          = 0x7b,
	I64_ADD             = 0x7c,
	I64_SUB             = 0x7d,
	I64_MUL             = 0x7e,
	I64_DIV_S           = 0x7f,
	I64_DIV_U           = 0x80,
	I64_REM_S           = 0x81,
	I64_REM_U           = 0x82,
	I64_AND             = 0x83,
	I64_OR              = 0x84,
	I64_XOR             = 0x85,
	I64_SHL             = 0x86,
	I64_SHR_S           = 0x87,
	I64_SHR_U           = 0x88,
	I64_ROTL            = 0x89,
	I64_ROTR            = 0x8a,
	F32_ABS             = 0x8b,
	F32_NEG             = 0x8c,
	F32_CEIL            = 0x8d,
	F32_FLOOR           = 0x8e,
	F32_TRUNC           = 0x8f,
	F32_NEAREST         = 0x90,
	F32_SQRT            = 0x91,
	F32_ADD             = 0x92,
	F32_SUB             = 0x93,
	F32_MUL             = 0x94,
	F32_DIV             = 0x95,
	F32_MIN             = 0x96,
	F32_MAX             = 0x97,
	F32_COPYSIGN        = 0x98,
	F64_ABS             = 0x99,
	F64_NEG             = 0x9a,
	F64_CEIL            = 0x9b,
	F64_FLOOR           = 0x9c,
	F64_TRUNC           = 0x9d,
	F64_NEAREST         = 0x9e,
	F64_SQRT            = 0x9f,
	F64_ADD             = 0xa0,
	F64_SUB             = 0xa1,
	F64_MUL             = 0xa2,
	F64_DIV             = 0xa3,
	F64_MIN             = 0xa4,
	F64_MAX             = 0xa5,
	F64_COPYSIGN        = 0xa6,
	I32_WRAP_I64        = 0xa7,
	I32_TRUNC_S_F32     = 0xa8,
	I32_TRUNC_U_F32     = 0xa9,
	I32_TRUNC_S_F64     = 0xaa,
	I32_TRUNC_U_F64     = 0xab,
	I64_EXTEND_S_I32    = 0xac,
	I64_EXTEND_U_I32    = 0xad,
	I64_TRUNC_S_F32     = 0xae,
	I64_TRUNC_U_F32     = 0xaf,
	I64_TRUNC_S_F64     = 0xb0,
	I64_TRUNC_U_F64     = 0xb1,
	F32_CONVERT_S_I32   = 0xb2,
	F32_CONVERT_U_I32   = 0xb3,
	F32_CONVERT_S_I64   = 0xb4,
	F32_CONVERT_U_I64   = 0xb5,
	F32_DEMOTE_F64      = 0xb6,
	F64_CONVERT_S_I32   = 0xb7,
	F64_CONVERT_U_I32   = 0xb8,
	F64_CONVERT_S_I64   = 0xb9,
	F64_CONVERT_U_I64   = 0xba,
	F64_PROMOTE_F32     = 0xbb,
	I32_REINTERPRET_F32 = 0xbc,
	I64_REINTERPRET_F64 = 0xbd,
	F32_REINTERPRET_I32 = 0xbe,
	F64_REINTERPRET_I64 = 0xbf,
}
} // namespace Wasm