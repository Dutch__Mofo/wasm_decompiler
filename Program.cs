﻿using System.Collections.Generic;
using System.IO;
using System;

using Opcode = Wasm.Opcode;
using System.Linq;

public class Program
{
	static int exit(string error)
	{
		Console.WriteLine(error);
		return 1;
	}
	
	enum Action
	{
		SHOW_FUNCTIONS,
		HEX_DUMP,
	}
	
	public static int Main(string[] args)
	{
		string filename = null;
		Action action = Action.SHOW_FUNCTIONS;
		
		foreach(string arg in args)
		{
			switch(arg)
			{
				case "-d": action = Action.HEX_DUMP; break;
				default:
				{
					if(arg[0] == '-') {
						return exit("Unkown command line option");
					}
					filename = arg;
				} break;
			}
		}
		
		if(filename == null) {
			return exit("Expected filename");
		}
		
		byte[] data;
		try {
			data = File.ReadAllBytes(filename);
		} catch {
			return exit("Couldn't open file");
		}
		
		Wasm.File file;
		#pragma warning disable 0168
		try {
			file = new WasmDecoder(data).parse();
		} catch(NotWasmFileException _) {
			return exit("Error: invalide file format");
		} catch(EarlyEndException _) {
			return exit("Error: file ended to early");
		} catch(InvalidOpcodeException _) {
			return exit("Error: invalid opcode");
		} catch(UnknownImportException _) {
			return exit("Error: unkown import");
		} catch(UnknownValueTypeException _) {
			return exit("Error: unkown value type");
		}
		#pragma warning restore 0168
		
		switch(action)
		{
			case Action.SHOW_FUNCTIONS: return showFunctions(file);
			case Action.HEX_DUMP: return hexDump(file);
			default: return 1;
		}
	}
	
	static int showFunctions(Wasm.File file)
	{
		if(file.functionSignatures == null) {
			return exit("Error: Type section missing");
		}
		if(file.functionTypes == null) {
			return exit("Error: Function section missing");
		}
		if(file.functionBodies == null) {
			return exit("Error: Code section missing");
		}
		if(file.exports == null) {
			return exit("Error: Export section missing");
		}
		
		for(int i = 0; i < file.functionBodies.Length; i += 1)
		{
			var signature = file.functionSignatures[ file.functionTypes[i] ];
			var function  = file.functionBodies[i];
			int indent = 1, curIndent;
			
			string name = file.exports.FirstOrDefault(e=> e.kind == Wasm.ExternalKind.FUNCTION && e.index == i).field;
			Console.WriteLine($"\nfunction {signature.returns.implode(", ")} {name} ({signature.parameters.implode(", ")})");
			
			foreach(var l in function.locals)
				Console.WriteLine($"  ({l.count} x {l.type})");
			
			foreach(var c in function.code)
			{
				curIndent = indent;
				switch(c.opcode)
				{
				case Opcode.BLOCK:
				case Opcode.LOOP:
				case Opcode.IF: indent += 1; break;
				
				case Opcode.ELSE: curIndent = Math.Max(0, curIndent - 1); break;
				case Opcode.END: indent = Math.Max(0, indent - 1); goto case Opcode.ELSE;
				}
				Console.WriteLine(new string(' ', curIndent * 2) + c);
			}
		}
		return 0;
	}
	
	static int hexDump(Wasm.File file)
	{
		log(0x6d736100, "file identifier '\\0asm'");
		log((int)file.version, "version");
		
		if(file.functionSignatures != null) {
			log("Type section");
			log(file.functionSignatures.Length, "count");
			foreach(var t in file.functionSignatures)
			{
				log(t.form, "form");
				log(t.parameters.Length, "param_count");
				foreach(var p in t.parameters)
					log((int)p, $"param_type ({p})");
				log(t.returns.Length > 0 ? (byte)1 : (byte)0, "return_count");
				foreach(var r in t.returns)
					log((int)r, $"return_type ({r})");
			}
		}
		if(file.imports != null) {
			log("import section");
			log(file.imports.Length, "count");
			foreach(var i in file.imports)
			{
				log(i.module.Length, "module_len");
				log(i.module, $"module_str \"{i.module}\"");
				log(i.field.Length, "module_len");
				log(i.field, $"field_str \"{i.field}\"");
			}
		}
		
		
		
		return 0;
	}
	
	static void log(string data, object text)
	{
		foreach(char c in data)
			Console.Write($"{(byte)c:x4}");
		Console.WriteLine(" ; "+text);
	}
	
	static void log(byte i, object text)
		=> Console.WriteLine($"{i:x4}     ; {text}");
		
	static void log(int i, object text)
		=> Console.WriteLine($"{i:x8} ; {text}");
		
	static void log(object text)
		=> Console.WriteLine($"; {text}");
}

static class Extensions
{
	public static string implode<T>(this IEnumerable<T> values, string glue)
		=> values.map(v=>v.ToString()).reduce((a,b)=>a+glue+b);

	public static IEnumerable<T1> map<T, T1>(this IEnumerable<T> list, Func<T,T1> action)
		{ foreach(var item in list) yield return action(item); }
	
	public static T reduce<T>(this IEnumerable<T> list, Func<T,T,T> action)
	{
		var enumerator = list.GetEnumerator();
		if(!enumerator.MoveNext()) return default(T);
		var aggregator = enumerator.Current;
		while(enumerator.MoveNext()) aggregator = action(aggregator, enumerator.Current);
		return aggregator;
	}
}  