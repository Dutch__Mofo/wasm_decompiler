namespace Wasm
{

public struct GlobalType
{
	public GlobalType(ValueType valueType, bool mutable)
	{
		this.valueType = valueType;
		this.mutable = mutable;
	}
	public ValueType valueType;
	public bool mutable;
}

public struct TableType
{
	public TableType(ValueType elementType, ResizableLimit limits)
	{
		this.elementType = elementType;
		this.limits = limits;
	}
	public ValueType elementType;
	public ResizableLimit limits;
}

public enum ValueType
{
	i32,
	i64,
	f32,
	f64,
	any_func,
	func,
	block_type, // empty (void)
}

}