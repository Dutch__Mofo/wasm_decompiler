namespace Wasm
{
	public abstract class Import
	{
		public Import(string module, string field)
		{
			this.module = module;
			this.field = field;
		}
		public string module, field;
	}
	
	public class ImportGlobalType : Import
	{
		public ImportGlobalType(string module, string field, GlobalType globalType) : base(module, field)
		{
			this.globalType = globalType;
		}
		public GlobalType globalType;
	}

	public class ImportTableType : Import
	{
		public ImportTableType(string module, string field, TableType tableType) : base(module, field)
		{
			this.tableType = tableType;
		}
		public TableType tableType;
	}

	public class ImportMemoryType : Import
	{
		public ImportMemoryType(string module, string field, ResizableLimit limits) : base(module, field)
		{
			this.limits = limits;
		}
		public ResizableLimit limits;
	}

	public class ImportFunctionType : Import
	{
		public ImportFunctionType(string module, string field, uint type) : base(module, field)
		{
			this.functionType = type;
		}
		public uint functionType;
	}
}