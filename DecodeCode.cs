using System.Collections.Generic;
using Wasm;

class DecodeCode
{
	Decoder D;
	List<Op> byteCode;
	
	public DecodeCode(Decoder D)
	{
		this.D = D;
	}
	
	public Op[] decode(uint end)
	{
		byteCode = new List<Op>();
		while(D.cursor < end) {
			byteCode.Add(decodeNext());
		}
		return byteCode.ToArray();
	}
	
	public Op decodeNext()
	{
		// ... means opcode's reserved for later use
		
		switch(D.next())
		{
		case 0x00: return O(Opcode.UNREACHABLE);
		case 0x01: return O(Opcode.NOP);
		case 0x02: return O(Opcode.BLOCK);
		case 0x03: return O(Opcode.LOOP);
		case 0x04: return O(Opcode.IF);
		case 0x05: return O(Opcode.ELSE);
		/* ... */
		case 0x0b: return O(Opcode.END);
		case 0x0c: return O(Opcode.BR,    D.parseVarint7());
		case 0x0d: return O(Opcode.BR_IF, D.parseVarint7());
		case 0x0e: return parseBrTable();
		case 0x0f: return O(Opcode.RETURN);
		case 0x10: return O(Opcode.CALL,          D.parseVaruint32());
		case 0x11: return O(Opcode.CALL_INDIRECT, D.parseVaruint32(), D.parseI1());
		/* ... */
		case 0x1a: return O(Opcode.DROP);
		case 0x1b: return O(Opcode.SELECT);
		/* ... */
		case 0x20: return O(Opcode.GET_LOCAL,  D.parseVaruint32());
		case 0x21: return O(Opcode.SET_LOCAL,  D.parseVaruint32());
		case 0x22: return O(Opcode.TEE_LOCAL,  D.parseVaruint32());
		case 0x23: return O(Opcode.GET_GLOBAL, D.parseVaruint32());
		case 0x24: return O(Opcode.SET_GLOBAL, D.parseVaruint32());
		/* ... */
		case 0x28: return O(Opcode.I32_LOAD,     parseMemImmediate());
		case 0x29: return O(Opcode.I64_LOAD,     parseMemImmediate());
		case 0x2a: return O(Opcode.F32_LOAD,     parseMemImmediate());
		case 0x2b: return O(Opcode.F64_LOAD,     parseMemImmediate());
		case 0x2c: return O(Opcode.I32_LOAD8_S,  parseMemImmediate());
		case 0x2d: return O(Opcode.I32_LOAD8_U,  parseMemImmediate());
		case 0x2e: return O(Opcode.I32_LOAD16_S, parseMemImmediate());
		case 0x2f: return O(Opcode.I32_LOAD16_U, parseMemImmediate());
		case 0x30: return O(Opcode.I64_LOAD8_S,  parseMemImmediate());
		case 0x31: return O(Opcode.I64_LOAD8_U,  parseMemImmediate());
		case 0x32: return O(Opcode.I64_LOAD16_S, parseMemImmediate());
		case 0x33: return O(Opcode.I64_LOAD16_U, parseMemImmediate());
		case 0x34: return O(Opcode.I64_LOAD32_S, parseMemImmediate());
		case 0x35: return O(Opcode.I64_LOAD32_U, parseMemImmediate());
		case 0x36: return O(Opcode.I32_STORE,    parseMemImmediate());
		case 0x37: return O(Opcode.I64_STORE,    parseMemImmediate());
		case 0x38: return O(Opcode.F32_STORE,    parseMemImmediate());
		case 0x39: return O(Opcode.F64_STORE,    parseMemImmediate());
		case 0x3a: return O(Opcode.I32_STORE8,   parseMemImmediate());
		case 0x3b: return O(Opcode.I32_STORE16,  parseMemImmediate());
		case 0x3c: return O(Opcode.I64_STORE8,   parseMemImmediate());
		case 0x3d: return O(Opcode.I64_STORE16,  parseMemImmediate());
		case 0x3e: return O(Opcode.I64_STORE32,  parseMemImmediate());
		
		case 0x3f: D.next(); return O(Opcode.CURRENT_MEMORY, D.parseI1());
		case 0x40: D.next(); return O(Opcode.GROW_MEMORY,    D.parseI1());
		
		case 0x41: return O(Opcode.I32_CONST, D.parseVaruint32());
		case 0x42: return O(Opcode.I64_CONST, D.parseVaruint64());
		case 0x43: return O(Opcode.F32_CONST, D.parseF32());
		case 0x44: return O(Opcode.F64_CONST, D.parseF64());
		
		case 0x45: return O(Opcode.I32_EQZ);
		case 0x46: return O(Opcode.I32_EQ);
		case 0x47: return O(Opcode.I32_NE);
		case 0x48: return O(Opcode.I32_LT_S);
		case 0x49: return O(Opcode.I32_LT_U);
		case 0x4a: return O(Opcode.I32_GT_S);
		case 0x4b: return O(Opcode.I32_GT_U);
		case 0x4c: return O(Opcode.I32_LE_S);
		case 0x4d: return O(Opcode.I32_LE_U);
		case 0x4e: return O(Opcode.I32_GE_S);
		case 0x4f: return O(Opcode.I32_GE_U);
		case 0x50: return O(Opcode.I64_EQZ);
		case 0x51: return O(Opcode.I64_EQ);
		case 0x52: return O(Opcode.I64_NE);
		case 0x53: return O(Opcode.I64_LT_S);
		case 0x54: return O(Opcode.I64_LT_U);
		case 0x55: return O(Opcode.I64_GT_S);
		case 0x56: return O(Opcode.I64_GT_U);
		case 0x57: return O(Opcode.I64_LE_S);
		case 0x58: return O(Opcode.I64_LE_U);
		case 0x59: return O(Opcode.I64_GE_S);
		case 0x5a: return O(Opcode.I64_GE_U);
		case 0x5b: return O(Opcode.F32_EQ);
		case 0x5c: return O(Opcode.F32_NE);
		case 0x5d: return O(Opcode.F32_LT);
		case 0x5e: return O(Opcode.F32_GT);
		case 0x60: return O(Opcode.F32_GE);
		case 0x5f: return O(Opcode.F32_LE);
		case 0x61: return O(Opcode.F64_EQ);
		case 0x62: return O(Opcode.F64_NE);
		case 0x63: return O(Opcode.F64_LT);
		case 0x64: return O(Opcode.F64_GT);
		case 0x65: return O(Opcode.F64_LE);
		case 0x66: return O(Opcode.F64_GE);
		case 0x67: return O(Opcode.I32_CLZ);
		case 0x68: return O(Opcode.I32_CTZ);
		case 0x69: return O(Opcode.I32_POPCNT);
		case 0x6a: return O(Opcode.I32_ADD);
		case 0x6b: return O(Opcode.I32_SUB);
		case 0x6c: return O(Opcode.I32_MUL);
		case 0x6d: return O(Opcode.I32_DIV_S);
		case 0x6e: return O(Opcode.I32_DIV_U);
		case 0x6f: return O(Opcode.I32_REM_S);
		case 0x70: return O(Opcode.I32_REM_U);
		case 0x71: return O(Opcode.I32_AND);
		case 0x72: return O(Opcode.I32_OR);
		case 0x73: return O(Opcode.I32_XOR);
		case 0x74: return O(Opcode.I32_SHL);
		case 0x75: return O(Opcode.I32_SHR_S);
		case 0x76: return O(Opcode.I32_SHR_U);
		case 0x77: return O(Opcode.I32_ROTL);
		case 0x78: return O(Opcode.I32_ROTR);
		case 0x79: return O(Opcode.I64_CLZ);
		case 0x7a: return O(Opcode.I64_CTZ);
		case 0x7b: return O(Opcode.I64_POPCNT);
		case 0x7c: return O(Opcode.I64_ADD);
		case 0x7d: return O(Opcode.I64_SUB);
		case 0x7e: return O(Opcode.I64_MUL);
		case 0x7f: return O(Opcode.I64_DIV_S);
		case 0x80: return O(Opcode.I64_DIV_U);
		case 0x81: return O(Opcode.I64_REM_S);
		case 0x82: return O(Opcode.I64_REM_U);
		case 0x83: return O(Opcode.I64_AND);
		case 0x84: return O(Opcode.I64_OR);
		case 0x85: return O(Opcode.I64_XOR);
		case 0x86: return O(Opcode.I64_SHL);
		case 0x87: return O(Opcode.I64_SHR_S);
		case 0x88: return O(Opcode.I64_SHR_U);
		case 0x89: return O(Opcode.I64_ROTL);
		case 0x8a: return O(Opcode.I64_ROTR);
		case 0x8b: return O(Opcode.F32_ABS);
		case 0x8c: return O(Opcode.F32_NEG);
		case 0x8d: return O(Opcode.F32_CEIL);
		case 0x8e: return O(Opcode.F32_FLOOR);
		case 0x8f: return O(Opcode.F32_TRUNC);
		case 0x90: return O(Opcode.F32_NEAREST);
		case 0x91: return O(Opcode.F32_SQRT);
		case 0x92: return O(Opcode.F32_ADD);
		case 0x93: return O(Opcode.F32_SUB);
		case 0x94: return O(Opcode.F32_MUL);
		case 0x95: return O(Opcode.F32_DIV);
		case 0x96: return O(Opcode.F32_MIN);
		case 0x97: return O(Opcode.F32_MAX);
		case 0x98: return O(Opcode.F32_COPYSIGN);
		case 0x99: return O(Opcode.F64_ABS);
		case 0x9a: return O(Opcode.F64_NEG);
		case 0x9b: return O(Opcode.F64_CEIL);
		case 0x9c: return O(Opcode.F64_FLOOR);
		case 0x9d: return O(Opcode.F64_TRUNC);
		case 0x9e: return O(Opcode.F64_NEAREST);
		case 0x9f: return O(Opcode.F64_SQRT);
		case 0xa0: return O(Opcode.F64_ADD);
		case 0xa1: return O(Opcode.F64_SUB);
		case 0xa2: return O(Opcode.F64_MUL);
		case 0xa3: return O(Opcode.F64_DIV);
		case 0xa4: return O(Opcode.F64_MIN);
		case 0xa5: return O(Opcode.F64_MAX);
		case 0xa6: return O(Opcode.F64_COPYSIGN);
		
		case 0xa7: return O(Opcode.I32_WRAP_I64);
		case 0xa8: return O(Opcode.I32_TRUNC_S_F32);
		case 0xa9: return O(Opcode.I32_TRUNC_U_F32);
		case 0xaa: return O(Opcode.I32_TRUNC_S_F64);
		case 0xab: return O(Opcode.I32_TRUNC_U_F64);
		case 0xac: return O(Opcode.I64_EXTEND_S_I32);
		case 0xad: return O(Opcode.I64_EXTEND_U_I32);
		case 0xae: return O(Opcode.I64_TRUNC_S_F32);
		case 0xaf: return O(Opcode.I64_TRUNC_U_F32);
		case 0xb0: return O(Opcode.I64_TRUNC_S_F64);
		case 0xb1: return O(Opcode.I64_TRUNC_U_F64);
		case 0xb2: return O(Opcode.F32_CONVERT_S_I32);
		case 0xb3: return O(Opcode.F32_CONVERT_U_I32);
		case 0xb4: return O(Opcode.F32_CONVERT_S_I64);
		case 0xb5: return O(Opcode.F32_CONVERT_U_I64);
		case 0xb6: return O(Opcode.F32_DEMOTE_F64);
		case 0xb7: return O(Opcode.F64_CONVERT_S_I32);
		case 0xb8: return O(Opcode.F64_CONVERT_U_I32);
		case 0xb9: return O(Opcode.F64_CONVERT_S_I64);
		case 0xba: return O(Opcode.F64_CONVERT_U_I64);
		case 0xbb: return O(Opcode.F64_PROMOTE_F32);
		case 0xbc: return O(Opcode.I32_REINTERPRET_F32);
		case 0xbd: return O(Opcode.I64_REINTERPRET_F64);
		case 0xbe: return O(Opcode.F32_REINTERPRET_I32);
		case 0xbf: return O(Opcode.F64_REINTERPRET_I64);
		default: throw new InvalidOpcodeException();
		}
	}
	
	Op parseBrTable()
	{
		uint targetCount = D.parseVaruint32();
		uint[] targets = new uint[targetCount];
		for(uint i = 0; i < targetCount; i += 1) {
			targets[i] = D.parseVaruint32();
		}
		uint defaultTarget = D.parseVaruint32();
		return new OpBrTable(Opcode.BR_TABLE, defaultTarget, targets);
	}
	
	MemImmidiate parseMemImmediate()
		=> new MemImmidiate(D.parseVaruint32(), D.parseVaruint32());
	
	Op O(Opcode opcode) => new Op(opcode);
	Op O(Opcode opcode, bool i) => new OpI1(opcode, i);
	Op O(Opcode opcode, byte i) => new OpI8(opcode, i);
	Op O(Opcode opcode, uint i) => new OpI32(opcode, i);
	Op O(Opcode opcode, ulong i) => new OpI64(opcode, i);
	Op O(Opcode opcode, float f) => new OpF32(opcode, f);
	Op O(Opcode opcode, double d) => new OpF64(opcode, d);
	Op O(Opcode opcode, MemImmidiate immidiate) => new OpMemImmidiate(opcode, immidiate);
	Op O(Opcode opcode, uint i32, bool i1) => new OpI32_I1(opcode, i32, i1);
}